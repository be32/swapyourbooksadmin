import {authReducer} from './auth';
import {currentUsersReducer} from './currentUsers';
import {rootsReducer} from './roots';
import {usersReducer} from './users';
import {categoriesReducer} from './categories';
import {locationsReducer} from './locations';
import {prosReducer} from './pros';
import {proFormFillingReducer} from './proFormFilling';

export {
    authReducer,
    currentUsersReducer,
    rootsReducer,
    usersReducer,
    categoriesReducer,
    locationsReducer,
    prosReducer,
    proFormFillingReducer
}
