import React, {Component} from 'react';
import './adminLogin.css';
import * as PropTypes from 'prop-types';
import {Formik} from "formik";
import {Misc} from "../../config";
import {Button, TextField} from "@material-ui/core";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import Firebase from '../../config/services/firebase';

const fireAuth = Firebase.auth();
/*
* "text": "Eonsectetur.\r\n",
                    "userId": "5d116fb09832a30bfa0db137",
                    "name": "Christie Conway",
                    "avatar": "http://placehold.it/32x32",
                    "creation_date": "2019-06-24T06:52:34 -01:00"
* */

class AdminLogin extends Component {

    loginByEmail = (creeds: {email: string, password: string}, callback) => {
        fireAuth.signInWithEmailAndPassword(creeds.email, creeds.password).then(value => {
           console.log('login logs = ', value);
           callback(value);
        });
    };

    render() {
        const {message, currentUser, previousUserId} = this.props;
        const {validateLoginForm} = Misc;

        return (
            <div className="loginComponentMainDiv">
                <span className="titleLoginPage">Swap Your books </span>
                <span className="titleLoginPage smallLoginTitle">Session Administrateur</span>
               <div className="loginFormContainer shadow">

                   <Formik
                       initialValues={{
                           email: undefined,
                           password: undefined
                       }}
                       validate={validateLoginForm}
                       onSubmit={(values, {setSubmitting}) => {
                           this.loginByEmail(values, (res) => {
                               setSubmitting(false);
                           });
                       }}
                   >
                       {({
                             values,
                             errors,
                             touched,
                             handleChange,
                             handleBlur,
                             handleSubmit,
                             isSubmitting,
                         }) => (
                           <MuiThemeProvider theme={Misc.THEME}>
                               <form onSubmit={handleSubmit} className="fields noBorder">

                                   <TextField
                                       id="outlined-email-input"
                                       label="E-mail"
                                       className="text"
                                       type="email"
                                       name="email"
                                       // autoComplete="email"
                                       inputProps={{
                                           className: 'inputClassStyle'
                                       }}
                                       margin="normal"
                                       variant="outlined"
                                       onChange={handleChange}
                                       onBlur={handleBlur}
                                       error={!!(errors.email && touched.email && errors.email)}
                                       helperText={errors.email && touched.email && errors.email}
                                       value={values.email || ''}
                                   />
                                   <TextField
                                       id="outlined-password-input"
                                       label="Mot de pass"
                                       className="text"
                                       type="password"
                                       name="password"
                                       color="secondary"
                                       // autoComplete="password"
                                       margin="normal"
                                       variant="outlined"
                                       onChange={handleChange}
                                       onBlur={handleBlur}
                                       value={values.password || ''}
                                       error={!!(errors.password && touched.password && errors.password)}
                                       helperText={errors.password && touched.password && errors.password}
                                   />
                                   <div className="buttonContainer">
                                       <Button type="submit" disabled={Object.keys(errors).length > 0 || isSubmitting}
                                               color="primary" variant="contained" className="signBtn">
                                           Se connecter
                                       </Button>
                                   </div>
                               </form>
                           </MuiThemeProvider>
                       )}
                   </Formik>
               </div>
            </div>
        );
    }
}

AdminLogin.propTypes = {
    message: PropTypes.object,
    currentUser: PropTypes.object,
    previousUserId: PropTypes.string
};

export default AdminLogin;
