import React, {Component} from 'react';
import './usersRequests.css';
import * as PropTypes from 'prop-types';
import {Misc} from "../../../../../config";
import {Button, Paper, Table, TableBody, TableCell, TableHead, TableRow, withStyles} from "@material-ui/core";
import {VerificationDialogWrapper} from "../../../../dialogs/verificationDialogWrapper";
import {NewMessageDialogWrapper} from "../../../../dialogs";


/*
* "text": "Eonsectetur.\r\n",
                    "userId": "5d116fb09832a30bfa0db137",
                    "name": "Christie Conway",
                    "avatar": "http://placehold.it/32x32",
                    "creation_date": "2019-06-24T06:52:34 -01:00"
* */

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);


const styles = theme => ({
    root: {
        // width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
            maxWidth: '90%'
        },
    },
});

class UsersRequests extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isMobile: false,
            dialogOn: false,
            requests: [
                {
                    "_id": "5d13543e51a669cebd08dcbb",
                    "name": "Mathews Duran",
                    "about": "Proident sint tempor aute voluptate adipisicing aliquip. Excepteur magna aute veniam cupidatat duis Lorem fugiat irure sint nulla sit. Nostrud consequat ut Lorem aliquip sunt aliquip eu magna id commodo nisi. Ad est nulla nostrud officia mollit. Officia sunt nostrud culpa non laboris dolor eu dolore occaecat nulla nostrud elit ut. Cupidatat non ut nostrud quis minim cupidatat reprehenderit.\r\n"
                },
                {
                    "_id": "5d13543e09e8f6df4ca20db5",
                    "name": "Franklin Travis",
                    "about": "Commodo ipsum exercitation duis nulla duis id voluptate consectetur officia. Irure minim non ex officia eiusmod ea nostrud tempor irure veniam esse dolore. Fugiat exercitation quis sit proident veniam do qui eiusmod. Voluptate enim qui ullamco velit elit nisi cupidatat nulla sint nostrud ipsum. Ad labore exercitation et duis. Excepteur culpa est commodo pariatur anim dolore laboris nisi consectetur dolor irure sit magna.\r\n"
                },
                {
                    "_id": "5d13543e6b581f44506a7ff2",
                    "name": "Ronda Sampson",
                    "about": "Laborum nisi culpa laborum labore ex id nostrud commodo culpa. Dolor laboris cillum consequat pariatur ea cillum sunt consectetur. Pariatur pariatur sit deserunt ullamco ad occaecat sit nulla. Qui in non culpa velit excepteur sit deserunt reprehenderit est. Laborum velit mollit veniam ut mollit adipisicing eu ad minim. Dolor labore laboris aliqua magna est pariatur elit veniam ea Lorem veniam consequat.\r\n"
                },
                {
                    "_id": "5d13543e04add4ad8048c8b9",
                    "name": "Terri Shepard",
                    "about": "Occaecat commodo officia duis exercitation do eiusmod commodo elit. Ipsum eu minim deserunt incididunt exercitation minim labore velit ad pariatur Lorem. Velit ipsum tempor enim do eiusmod ex sit velit. Lorem irure do fugiat duis occaecat ex nulla ut id duis nulla Lorem aute. Occaecat ea sit tempor consequat. Eiusmod labore sunt et consectetur consequat deserunt magna ea quis nulla qui occaecat magna.\r\n"
                },
                {
                    "_id": "5d13543e230cc22a7dec2daf",
                    "name": "Heath Bradford",
                    "about": "Non incididunt ipsum esse culpa mollit magna. Ullamco aute nisi mollit in fugiat laborum sit nostrud in. Excepteur minim est in nisi aliqua non amet. Ullamco sunt ipsum et Lorem aliqua. Exercitation commodo voluptate sit excepteur. Velit nostrud esse commodo non.\r\n"
                },
                {
                    "_id": "5d13543e56ea1f074d21c818",
                    "name": "Effie Sloan",
                    "about": "Qui culpa eiusmod ea irure laboris dolor ex ea tempor. Commodo cupidatat sunt ullamco amet commodo Lorem commodo do proident et laboris laborum. Laboris adipisicing nulla cupidatat aute laborum mollit sit anim. Dolore occaecat occaecat eiusmod cillum consectetur non sunt.\r\n"
                }
            ]
        }
    }

    setDialogOn = () => {
        this.setState({
            dialogOn: true
        })
    }

    setDialogClose = () => {
        this.setState({
            dialogOn: false
        })
    }

    componentWillMount() {
        this.setViewType()
    };

    componentDidMount() {
        window.addEventListener("resize", this.setViewType);
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.setViewType);
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({
                    isMobile: true
                });
                return;
            case 'sm':
                this.setState({
                    isMobile: true
                });
                return;
            case 'md':
                this.setState({
                    isMobile: false
                });
                return;
            case 'lg':
                this.setState({
                    isMobile: false
                });
                return;
            case 'xl':
                this.setState({
                    isMobile: false
                });
                return;
            default:
                return;
        }
    };

    render() {
        const {classes} = this.props;
        const {isMobile, requests, dialogOn} = this.state;
        return (
            <div className="mainViewSubPages" style={{paddingLeft: (!isMobile ? 360 : 0)}}>
                <span className="listsPageTitle">Demandes des utilisateurs</span>
                <Paper className={'mainViewContainer' + " " + classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <CustomTableCell align="right">Nom d'utilisateur</CustomTableCell>
                                <CustomTableCell align="right">Message</CustomTableCell>
                                <CustomTableCell align="right">Action</CustomTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {/*
                            {
                    "_id": "5d12607a84f1ab5c94f84eff",
                    "name": "deserunt culpa",
                    "toExchange": "qui qui",
                    "verified": true,
                    "picture": "http://placehold.it/332x332",
                    "userPicture": "http://placehold.it/32x32",
                    "isbn": 8876,
                    "userName": "Shawna Nielsen",
                    "creation_date": "2016-09-14T08:07:43 -01:00"
                }
                            */}
                            {requests.map((row, index) => (
                                <TableRow className={classes.row} key={row._id}>
                                    <CustomTableCell align="right">{row.name}</CustomTableCell>
                                    <CustomTableCell align="right">
                                        <span style={{maxWidth: '15vw'}}>{row.about}</span>
                                    </CustomTableCell>
                                    <CustomTableCell align="right">
                                        <Button variant={"contained"} onClick={this.setDialogOn}>
                                            Répendre
                                        </Button>
                                    </CustomTableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
                <NewMessageDialogWrapper open={dialogOn} setOpen={this.setDialogOn} setClose={this.setDialogClose}/>
            </div>
        );
    }
}

UsersRequests.propTypes = {
    message: PropTypes.object,
    currentUser: PropTypes.object,
    previousUserId: PropTypes.string
};

export default withStyles(styles)(UsersRequests);
