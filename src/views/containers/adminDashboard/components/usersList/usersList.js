import React, {Component} from 'react';
import './usersList.css';
import * as PropTypes from 'prop-types';
import {Misc} from "../../../../../config";
import {Button, Paper, Table, TableBody, TableCell, TableHead, TableRow, withStyles} from "@material-ui/core";
import {VerificationDialogWrapper} from "../../../../dialogs/verificationDialogWrapper";
import * as UsersService from '../../../../../config/services/users';

/*
* "text": "Eonsectetur.\r\n",
                    "userId": "5d116fb09832a30bfa0db137",
                    "name": "Christie Conway",
                    "avatar": "http://placehold.it/32x32",
                    "creation_date": "2019-06-24T06:52:34 -01:00"
* */

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);


const styles = theme => ({
    root: {
        // width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
});

class UsersList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isMobile: false,
            users: [],
            dialogOn: false,
            dialogMessage: '',
            actionType: '',
            user: null
        }
    }

    setDialogOn = () => {
        this.setState({
            dialogOn: true
        })
    }

    setDialogClose = () => {
        this.setState({
            dialogOn: false
        })
    }

    setDialogMessageAndUser = (message, type, user) => {
        this.setState({
            dialogMessage: message,
            actionType: type,
            user: user
        })
    };


    componentWillMount() {
        this.setViewType()
    };

    componentDidMount() {
        window.addEventListener("resize", this.setViewType);

        UsersService.getAllUsers((succ, res, err) => {
           if (succ) {
               let usersCopy = this.state.users.slice();
               let exIdx = usersCopy.findIndex(x => res.id === x.id);
               if (exIdx === -1)
                   this.setState({
                       users: [...this.state.users, res]
                   });
               else {
                   usersCopy[exIdx] = res;
                   this.setState({
                       users: usersCopy
                   });
               }
           }
        });
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.setViewType);
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({
                    isMobile: true
                });
                return;
            case 'sm':
                this.setState({
                    isMobile: true
                });
                return;
            case 'md':
                this.setState({
                    isMobile: false
                });
                return;
            case 'lg':
                this.setState({
                    isMobile: false
                });
                return;
            case 'xl':
                this.setState({
                    isMobile: false
                });
                return;
            default:
                return;
        }
    };

    render() {
        const {message, currentUser, previousUserId, classes} = this.props;
        const {isMobile, users, dialogOn, dialogMessage, user, actionType} = this.state;
        return users ? (
            <div className="mainViewSubPages" style={{paddingLeft: (!isMobile ? 360 : 0)}}>
                <span className="listsPageTitle">Gestion des utilisateurs</span>
                <Paper className={'mainViewContainer' + " " + classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <CustomTableCell>Image</CustomTableCell>
                                <CustomTableCell align="right">Nom</CustomTableCell>
                                <CustomTableCell align="right">Sexe</CustomTableCell>
                                <CustomTableCell align="right">Age</CustomTableCell>
                                <CustomTableCell align="right">E-mail</CustomTableCell>
                                {/*<CustomTableCell align="right">Tél.</CustomTableCell>*/}
                                <CustomTableCell align="right">Suspendre</CustomTableCell>
                                <CustomTableCell align="right">Supprimer.</CustomTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {/*
                            {
                    "_id": "5d134f3f360db757988f1dc0",
                    "picture": "http://placehold.it/32x32",
                    "age": 31,
                    "name": "Strong Sawyer",
                    "gender": "male",
                    "email": "strongsawyer@autograte.com",
                    "phone": "+1 (984) 402-2341"
                            */}
                            {users.map((row, index) => (
                                <TableRow className={classes.row} key={row.id}>
                                    <CustomTableCell component="th" scope="row">
                                        {row.photoURL ?
                                            <img src={row.photoURL} width={100} height={70} alt={row.name}/> : ""}
                                    </CustomTableCell>
                                    <CustomTableCell align="right">{row.username ? row.username : ""}</CustomTableCell>
                                    <CustomTableCell align="right">{row.gender ? row.gender : ""}</CustomTableCell>
                                    <CustomTableCell align="right">{row.birthdate ? row.birthdate.toDate().toLocaleString() : ""}</CustomTableCell>
                                    <CustomTableCell align="right">{row.email ? row.email : ""}</CustomTableCell>
                                    {/*<CustomTableCell align="right">{row.phone}</CustomTableCell>*/}
                                    <CustomTableCell align="right">
                                        <Button variant={"contained"} onClick={() => {
                                            this.setDialogMessageAndUser(`Voulez-vous vraiment ${row.suspended ? 'activer' : 'suspendre'} cet utilisateur ? `, 'suspend', row);
                                            this.setDialogOn();
                                        }}>
                                            {row.suspended ? 'Activer' : 'Suspendre'}
                                        </Button>
                                    </CustomTableCell>
                                    <CustomTableCell align="right">
                                        <Button variant={"contained"} onClick={() => {
                                            this.setDialogMessageAndUser('Voulez-vous vraiment supprimer cet utilisateur ? ', 'delete', row);
                                            this.setDialogOn();
                                        }}>
                                            Supprimer
                                        </Button>
                                    </CustomTableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
                <VerificationDialogWrapper message={dialogMessage} open={dialogOn}
                                           setClose={this.setDialogClose} setOpen={this.setDialogOn}
                                           user={user} type={actionType}
                />
            </div>
        ) : <div/>;
    }
}

UsersList.propTypes = {
    message: PropTypes.object,
    currentUser: PropTypes.object,
    previousUserId: PropTypes.string
};

export default withStyles(styles)(UsersList);
