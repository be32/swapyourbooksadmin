import React, {Component} from 'react';
import './eventsList.css';
import * as PropTypes from 'prop-types';
import {Misc} from "../../../../../config";
import {Button, Paper, Table, TableBody, TableCell, TableHead, TableRow, withStyles} from "@material-ui/core";
import {VerificationDialogWrapper} from "../../../../dialogs/verificationDialogWrapper";
import {getAllEvents} from '../../../../../config/services/events';


/*
* "text": "Eonsectetur.\r\n",
                    "userId": "5d116fb09832a30bfa0db137",
                    "name": "Christie Conway",
                    "avatar": "http://placehold.it/32x32",
                    "creation_date": "2019-06-24T06:52:34 -01:00"
* */

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);


const styles = theme => ({
    root: {
        // width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
});

class EventsList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isMobile: false,
            events: [],
            dialogOn: false
        }
    }

    setDialogOn = (event) => {
        this.setState({
            dialogOn: true,
            eventChosen: event,
            dialogMessage: `Voulez-vous vraiment ${event.verified ? 'annuler la vérification de' : 'vérifié'} cet événement? `
        })
    };

    setDialogClose = () => {
        this.setState({
            dialogOn: false
        })
    }

    componentWillMount() {
        this.setViewType()
    };

    componentDidMount() {
        window.addEventListener("resize", this.setViewType);

        getAllEvents((succ, res, err) => {
            if (succ) {
                let eventsCopy = this.state.events.slice();
                let exIdx = eventsCopy.findIndex(x => res.id === x.id);
                if (exIdx === -1)
                    this.setState({
                        events: [...this.state.events, res]
                    });
                else {
                    eventsCopy[exIdx] = res;
                    this.setState({
                        events: eventsCopy
                    });
                }
            }

        });
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.setViewType);
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({
                    isMobile: true
                });
                return;
            case 'sm':
                this.setState({
                    isMobile: true
                });
                return;
            case 'md':
                this.setState({
                    isMobile: false
                });
                return;
            case 'lg':
                this.setState({
                    isMobile: false
                });
                return;
            case 'xl':
                this.setState({
                    isMobile: false
                });
                return;
            default:
                return;
        }
    };

    render() {
        const {classes} = this.props;
        const {isMobile, events, dialogOn, eventChosen, dialogMessage} = this.state;
        return (
            <div className="mainViewSubPages" style={{paddingLeft: (!isMobile ? 360 : 0)}}>
                <span className="listsPageTitle">Vérification des événements</span>
                <Paper className={'mainViewContainer' + " " + classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <CustomTableCell>Image</CustomTableCell>
                                <CustomTableCell align="right">Nom de l'événement</CustomTableCell>
                                <CustomTableCell align="right">Utilisateur</CustomTableCell>
                                <CustomTableCell align="right">Vérifier</CustomTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {events.map((row, index) => (
                                <TableRow className={classes.row} key={row.id}>
                                    <CustomTableCell component="th" scope="row">
                                        {row.picture ?
                                            <img src={row.picture} width={100} height={70} alt={row.name}/> : ""}
                                    </CustomTableCell>
                                    <CustomTableCell align="right">{row.name}</CustomTableCell>
                                    <CustomTableCell align="right">{row.username}</CustomTableCell>
                                    <CustomTableCell align="right">
                                        <Button variant={"contained"} onClick={() => {this.setDialogOn(row)}}>
                                            {row.verified ? 'Annuler vérif' : 'Vérifier'}
                                        </Button>
                                    </CustomTableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
                <VerificationDialogWrapper message={dialogMessage} open={dialogOn}
                                           setClose={this.setDialogClose} event={true} setOpen={this.setDialogOn}
                                           user={eventChosen}
                />
            </div>
        );
    }
}

EventsList.propTypes = {
    message: PropTypes.object,
    currentUser: PropTypes.object,
    previousUserId: PropTypes.string
};

export default withStyles(styles)(EventsList);
