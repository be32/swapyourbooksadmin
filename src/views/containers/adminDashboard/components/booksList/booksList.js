import React, {Component} from 'react';
import './booksList.css';
import * as PropTypes from 'prop-types';
import {Misc} from "../../../../../config";
import {withStyles} from '@material-ui/core/styles';
import {
    Paper, IconButton,
    Menu,
    MenuItem,
    Snackbar,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow, Button
} from "@material-ui/core";
import {VerificationDialogWrapper} from "../../../../dialogs";
import {getAllBooks} from '../../../../../config/services/books';

/*
* "text": "Eonsectetur.\r\n",
                    "userId": "5d116fb09832a30bfa0db137",
                    "name": "Christie Conway",
                    "avatar": "http://placehold.it/32x32",
                    "creation_date": "2019-06-24T06:52:34 -01:00"
* */

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);


const styles = theme => ({
    root: {
        // width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
});

class BooksList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isMobile: false,
            books: [],
            dialogOn: false
        }
    }

    setDialogOn = (book) => {
        if (book)
        this.setState({
            dialogOn: true,
            book: book,
            dialogMessage: `Voulez-vous vraiment ${book.verified ? 'annuler la vérification de' : 'vérifié'} ce livre ? `
        });
        else this.setState({
            dialogOn: true
        });
    };

    setDialogClose = () => {
        this.setState({
            dialogOn: false
        })
    };

    componentWillMount() {
        this.setViewType()
    };

    componentDidMount() {
        window.addEventListener("resize", this.setViewType);

        getAllBooks((succ, res, err) => {
            if (succ) {
                let booksCopy = this.state.books.slice();
                let exIdx = booksCopy.findIndex(x => res.id === x.id);
                if (exIdx === -1)
                    this.setState({
                        books: [...this.state.books, res]
                    });
                else {
                    booksCopy[exIdx] = res;
                    this.setState({
                        books: booksCopy
                    });
                }
            }
        })
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.setViewType);
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({
                    isMobile: true
                });
                return;
            case 'sm':
                this.setState({
                    isMobile: true
                });
                return;
            case 'md':
                this.setState({
                    isMobile: false
                });
                return;
            case 'lg':
                this.setState({
                    isMobile: false
                });
                return;
            case 'xl':
                this.setState({
                    isMobile: false
                });
                return;
            default:
                return;
        }
    };

    render() {
        const {classes} = this.props;
        const {isMobile, books, dialogOn, book, dialogMessage} = this.state;
        return (
            <div className="mainViewSubPages" style={{paddingLeft: (!isMobile ? 360 : 0)}}>
                <span className="listsPageTitle">Vérification des livres</span>
                <Paper className={'mainViewContainer' + " " + classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <CustomTableCell>Image</CustomTableCell>
                                <CustomTableCell align="right">Nom du livre</CustomTableCell>
                                <CustomTableCell align="right">à échanger avec</CustomTableCell>
                                <CustomTableCell align="right">Utilisateur</CustomTableCell>
                                <CustomTableCell align="right">ISBN</CustomTableCell>
                                <CustomTableCell align="right">Vérifier</CustomTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {books.map((row, index) => (
                                <TableRow className={classes.row} key={row.id}>
                                    <CustomTableCell component="th" scope="row">
                                        {row.picture ?
                                            <img src={row.picture} width={100} height={70} alt={row.name}/> : ""}
                                    </CustomTableCell>
                                    <CustomTableCell align="right">{row.name}</CustomTableCell>
                                    <CustomTableCell align="right">{row.toExchange}</CustomTableCell>
                                    <CustomTableCell align="right">{row.username}</CustomTableCell>
                                    <CustomTableCell align="right">{row.isbn}</CustomTableCell>
                                    <CustomTableCell align="right">
                                        <Button variant={"contained"} onClick={() => {this.setDialogOn(row)}}>
                                            {row.verified ? 'Annuler vérif' : 'Vérifier'}
                                        </Button>
                                    </CustomTableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
                <VerificationDialogWrapper message={dialogMessage} open={dialogOn}
                                           setClose={this.setDialogClose} setOpen={this.setDialogOn} book={true} user={book}/>
            </div>
        );
    }
}

BooksList.propTypes = {
    message: PropTypes.object,
    currentUser: PropTypes.object,
    previousUserId: PropTypes.string
};

export default withStyles(styles)(BooksList);
