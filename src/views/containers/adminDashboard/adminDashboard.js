import React, {Component} from 'react';
import './adminDashboard.css';
import * as PropTypes from 'prop-types';
import {MuiThemeProvider} from "@material-ui/core";
import {Misc} from "../../../config";
import HeaderComponent from "../../components/header/headerComponent";
import SideNavComponent from "../../components/sideNav/sideNavComponent";

class AdminDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            left: false,
            right: false
        };
    }


    toggleDrawer = (side, open) => () => {
        this.setState({
            [side]: open,
        });
    };

    render() {
        const {children} = this.props;
        return (
            <MuiThemeProvider theme={Misc.THEME}>
                <div className="mainBackStyle">
                    <HeaderComponent openFilter={this.state.right} open={this.state.left}
                                     toggleDrawer={this.toggleDrawer.bind(this)}/>
                    <SideNavComponent open={this.state.left} toggleDrawer={this.toggleDrawer.bind(this)}/>
                    <div className="mainSpacer"/>
                    {children}
                </div>
            </MuiThemeProvider>
        );
    }
}

AdminDashboard.propTypes = {
    message: PropTypes.object,
    currentUser: PropTypes.object,
    previousUserId: PropTypes.string
};

export default AdminDashboard;
