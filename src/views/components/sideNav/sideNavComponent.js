import React, {Component} from 'react';
import './sideNavComponent.css';
import {IconButton, MuiThemeProvider} from "@material-ui/core";
import {withStyles} from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ChevronRight from '@material-ui/icons/ChevronRight';

import CloseIcon from '@material-ui/icons/Close';
import {Link} from '@reach/router';

import {connect} from "react-redux";
import {Misc} from "../../../config";


const styles = {
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
};

const settings = [
    {
        name: 'Gestion des utilisateurs',
        link: ''
    },
    {
        name: 'Vérification des livres',
        link: 'books'
    },
    {
        name: 'Vérification des événements',
        link: 'events'
    }
];


class SideNavComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isMobile: false
        }
    }

    componentWillMount() {
        this.setViewType()
    };

    componentDidMount() {
        window.addEventListener("resize", this.setViewType);
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.setViewType);
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({
                    isMobile: true
                });
                return;
            case 'sm':
                this.setState({
                    isMobile: true
                });
                return;
            case 'md':
                this.setState({
                    isMobile: false
                });
                return;
            case 'lg':
                this.setState({
                    isMobile: false
                });
                return;
            case 'xl':
                this.setState({
                    isMobile: false
                });
                return;
            default:
                return;
        }
    };


    render() {
        const {classes, pathname, isLoggedIn, isPro} = this.props;

        return (
            <div>
                <Drawer
                    variant={!this.state.isMobile ? "permanent" : "temporary"}
                    open={this.props.open} onClose={this.props.toggleDrawer('left', false)}>
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={this.props.toggleDrawer('left', false)}
                        onKeyDown={this.props.toggleDrawer('left', false)}
                        className="sideList"
                    >
                        <div className="mainSpacer"/>

                        <div className={classes.fullList}>
                            <span className="subTitle">Gestion administrative</span>
                            <br/>
                            <List>
                                {settings.map((text, index) => (
                                    <Link to={`/${text.link}`} key={index + text.link}>
                                        <ListItem button key={text.name} className="listBtn">
                                            <ListItemText primary={text.name}/>
                                            <ListItemIcon>
                                                <ChevronRight color="secondary"/>
                                            </ListItemIcon>
                                        </ListItem>
                                    </Link>
                                ))}
                            </List>
                        </div>
                    </div>
                </Drawer>
            </div>
        );
    }
}

export default withStyles(styles)(SideNavComponent);
