import React, {Component} from 'react';
import './bookDemandComponent.css';
import * as PropTypes from 'prop-types';


class BookDemandComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isMobile: false
        }
    }

    componentWillMount() {
    };

    componentDidMount() {
    };

    componentWillUnmount() {
    };


    render() {
        const {book} = this.props;
        const {isMobile} = this.state;

        /*
* {
                    "_id": "5d12607a1f03ef15e4c36237",
                    "name": "in in",
                    "toExchange": "labore eiusmod",
                    "verified": true,
                    "picture": "http://placehold.it/332x332",
                    "userPicture": "http://placehold.it/32x32",
                    "isbn": 9768,
                    "userName": "Keri Head",
                    "creation_date": "2019-03-17T08:00:08 -01:00"

* */
        return (
            <div className="mainSmallComponent shadow">
                <div className="picture"></div>
            </div>
        );
    }
}

BookDemandComponent.propTypes = {
    book: PropTypes.object,
};

export default BookDemandComponent;
