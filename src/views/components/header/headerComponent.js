import React, {Component} from 'react';
import ReactDom from 'react-dom';
import {navigate} from '@reach/router';
import './headerComponent.css';
import {Button, IconButton, Menu, MenuItem, Toolbar} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import {withStyles} from '@material-ui/core/styles'
import {Link} from "@reach/router";
import {CONST} from "../../../config";
import * as PropTypes from 'prop-types';
import Firebase from '../../../config/services/firebase';

const fireAuth = Firebase.auth();

const {pubSubConstants, localStorageIndexes} = CONST;
const pubSubJs = require('pubsub-js');


const styles = theme => ({
    root: {
        justifyContent: 'space-between',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        width: '100vw',
        backgroundColor: "#3d465c",
        zIndex: 1220,
        position: 'fixed',
        // boxShadow: '2px 2px 7px rgba(98, 163, 238, 0.63)'
},
    borderToHeader: {
        borderBottom: 'rgba(0, 0, 0, 0.1) solid 1px'
    },
    grow: {
        flexGrow: 1
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    }
});


class HeaderComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showFilter: false,
            toolbarHeight: 0,
            anchorEl: null
        }
    }

    componentWillMount() {
        pubSubJs.subscribe(pubSubConstants.showFilter, () => {
            this.setState({
                showFilter: true
            })
        });
        pubSubJs.subscribe(pubSubConstants.hideFilter, () => {
            this.setState({
                showFilter: false
            })
        });
    }

    componentDidMount(): void {
        this.setState({
            toolbarHeight: ReactDom.findDOMNode(this.toolBarRef).offsetHeight
        });
    }

    handleMenuClick = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleMenuClose = () => {
        this.setState({anchorEl: null});
    };

    handleLogout = () => {
        fireAuth.signOut().then((res) => {
            console.log(res);
            navigate('/admin-login');
        })
    };

    render() {
        const {classes, pathname, user} = this.props;

        return (
            <Toolbar className={classes.root}
                     ref={ref => this.toolBarRef = ref}>
                <div className="headerMaxWidth">
                    <div className="menu">
                        <IconButton color="secondary" aria-label="Menu"
                                    onClick={this.props.toggleDrawer('left', !this.props.open)}>
                            <MenuIcon/>
                        </IconButton>
                    </div>
                    <div className={"logo" + (pathname === "/" ? (" " + "hideLogo") : "")}>
                        <Link to="/">
                            <Button color="inherit" aria-label="Menu">
                               <span className="mainTitle"> Swap your books</span>
                            </Button>
                        </Link>
                    </div>


                    <div className={classes.grow}/>
                    <div className="logo">
                        <Button onClick={this.handleLogout} color="secondary" className={classes.button}>
                            Déconnexion
                        </Button>
                    </div>
                </div>
            </Toolbar>
        );
    }
}

HeaderComponent.propTypes = {
    toggleDrawer: PropTypes.func
};

export default withStyles(styles)(HeaderComponent);
