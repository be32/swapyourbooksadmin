import React, {Component} from 'react';
import './verificationDialogWrapper.css';
import * as PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Misc} from "../../../config";
import {deleteUser, suspendUser} from '../../../config/services/users';
import {deleteBook, verifyBook} from '../../../config/services/books';
import {deleteEvent, verifyEvent} from '../../../config/services/events';

export default class VerificationDialogWrapper extends Component {
    /*theme = useTheme();
    fullScreen = useMediaQuery(this.theme.breakpoints.down('sm'));*/
    constructor(props) {
        super(props);

        this.state = {
            fullScreen: false
        }
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({fullScreen: true});
                return;
            case 'sm':
                this.setState({fullScreen: true});

                return;
            case 'md':
                this.setState({fullScreen: false});

                return;
            case 'lg':
                this.setState({fullScreen: false});

                return;
            case 'xl':
                this.setState({fullScreen: false});

                return;
            default:
                return;
        }
    };


    componentDidMount(): void {
        // we call before we subscribe to fire it then we sub, so it fires each time we resize
        this.setViewType();

        window.addEventListener("resize", this.setViewType);
    }

    deleteUser = () => {
        deleteUser(this.props.user.id, () => {
            this.props.setClose();
        });
    };

    suspendUser = () => {
        suspendUser(this.props.user.id, !Boolean(this.props.user.suspended), () => {
            this.props.setClose();
        })
    };

    deleteBook = () => {
        if (this.props.book)
            deleteBook(this.props.user.id, () => {
                this.props.setClose();
            });
        else deleteEvent(this.props.user.id, () => {
            this.props.setClose();
        })
    };

    verifyBook = () => {
        if (this.props.book)
            verifyBook(this.props.user.id, !Boolean(this.props.user.verified), () => {
                this.props.setClose();
            });
        else verifyEvent(this.props.user.id, !Boolean(this.props.user.verified), () => {
            this.props.setClose();
        })
    };

    componentWillUnmount(): void {
        window.removeEventListener("resize", this.setViewType);
    }

    render() {
        const {open, setOpen, setClose, pro, event, navigate, message, type, user, book, ...other} = this.props;
        return (
            <div>
                <Dialog
                    fullScreen={this.state.fullScreen}
                    open={open}
                    onClose={setClose}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">{"Vous êtes sûr ? "}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>{message}</DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={setClose} color="secondary">
                            Annuler
                        </Button>
                        {/*Show book related buttons if it's called in books list else show users related buttons*/}

                        {book || event ? [
                                <Button key={3} onClick={this.deleteBook} color="secondary">
                                    Supprimer
                                </Button>,
                                <Button key={4} onClick={this.verifyBook} color="secondary" autoFocus>
                                    Oui
                                </Button>]
                            : type === 'delete' ? <Button onClick={this.deleteUser} color="secondary">
                                Supprimer
                            </Button> : <Button onClick={this.suspendUser} color="secondary" autoFocus>
                                {user && user.suspended ? 'Activer' : 'Suspendre'}
                            </Button>
                        }
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

VerificationDialogWrapper.propTypes = {
    className: PropTypes.string,
    open: PropTypes.bool,
    book: PropTypes.bool,
    event: PropTypes.bool,
    setOpen: PropTypes.func,
    setClose: PropTypes.func,
    navigate: PropTypes.func,
    message: PropTypes.string,
    type: PropTypes.string,
    user: PropTypes.object
};
