import {NewMessageDialogWrapper} from './newMessageDialogWrapper';
import {VerificationDialogWrapper} from './verificationDialogWrapper';

export {
    NewMessageDialogWrapper,
    VerificationDialogWrapper
}
