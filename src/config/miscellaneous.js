import React from 'react';
import {createMuiTheme} from "@material-ui/core";

const THEME = createMuiTheme({
    palette: {
        primary: {
            light: '#3d465c',
            main: '#353a4e',
            // dark: '#b0b2b0',
            contrastText: '#62a3ee',
        },
        secondary: {
            light: '#ffdbb9',
            main: '#fa7564',
            // dark: '#560e11',
            // contrastText: '#00FF48',
        }
    }, typography: {
        useNextVariants: true,
    }
});

const FORM_THEME = createMuiTheme({
    palette: {
        primary: {
            light: '#3d465c',
            main: '#353a4e',
            // dark: '#b0b2b0',
            contrastText: '#62a3ee',
        },
        secondary: {
            light: '#ffdbb9',
            main: '#fa7564',
            // dark: '#560e11',
            // contrastText: '#00FF48',
        }
    }, typography: {
        useNextVariants: true,
    }
});

function isURL(str) {
    var regex = /(((http|https):\/\/)|www.)(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    var pattern = new RegExp(regex);
    return pattern.test(str);
}

const getWindowDimensions = () => {
    const {innerWidth: width, innerHeight: height} = window;
    return {
        width,
        height
    };
};

const getSizeType = () => {
    let width = getWindowDimensions().width;

    if (width <= 599) {
        return 'xs';
    } else if (width >= 600 && width <= 959) {
        return 'sm';
    } else if (width >= 960 && width <= 1279) {
        return 'md';
    } else if (width >= 1280 && width <= 1919) {
        return 'lg';
    } else if (width >= 1920 && width <= 5000) {
        return 'xl';
    }
};

const validateEmail = (values, errors = {}) => {
    if (!values.email) {
        errors.email = 'L\'email est requis';
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
    ) {
        errors.email = 'Adresse e-mail invalide.';
    }
    return errors;
};

const validatePass = (values, errors = {}) => {
    if (!values.password) {
        errors.password = 'Mot de pass est requis.';
    } else if (values.password.length < 8) {
        errors.password = 'Le mot de passe doit comporter au moins 8 caractères.';
    }
    return errors;
};

const validateNewPass = (values, errors = {}) => {
    if (!values.newPassword) {
        errors.newPassword = 'Nouvelle mot de pass est requis.';
    } else if (values.password === values.newPassword) {
        errors.newPassword = 'Ne doit pas être le même que le mot de passe.';
    } else if (values.newPassword.length < 8) {
        errors.newPassword = 'Le mot de passe doit comporter au moins 8 caractères.';
    }
    return errors;
};

const isEqual = (objOne: any, objTwo: any) => {
    return Object.values(objOne).toString() === Object.values(objTwo).toString();
};

const validatePassTwo = (values, errors = {}) => {
    if (!values.password2) {
        errors.password2 = 'Doit être le même que le mot de passe.';
    } else if (values.password !== values.password2) {
        errors.password2 = 'Doit être le même que le mot de passe.';
    }
    return errors;
};

const getBusinessFeeRate = (rate) => {
    switch (rate) {
        case "C":
            return "Consultation";
        case "H":
            return "Horaire";
        case "F":
            return "Forfaitaire";
        case "Q":
            return "Quotas";
        case "J":
            return "Journalier";
        default:
            return "Négociable";
    }
};

const validateName = (values, errors = {}) => {
    if (!values.name) {
        errors.name = 'Nom est requis.';
    }
    return errors;
};

const validateLocation = (values, errors = {}) => {
    if (!values.locString) {
        errors.locString = 'Emplacement est requis.';
    }
    return errors;
};

const validateProName = (values, errors = {}) => {
    if (!values.businessName) {
        errors.businessName = 'Nom est requis.';
    }
    return errors;
};

const validateCategoryId = (values, errors = {}) => {
    if (!values.categoryId) {
        errors.categoryId = 'La catégorie est requis.';
    }
    return errors;
};

const validateLastName = (values, errors = {}) => {
    if (!values.lastName) {
        errors.lastName = 'Prénom est requis.';
    }
    return errors;
};

const validatePhone = (values, errors = {}) => {
    if (!values.phone) {
        errors.phone = 'Numéro de téléphone est requis.';
    } else if (values.phone.length < 8) {
        errors.phone = 'Le numéro de téléphone doit comporter au moins 8 caractères.';
    }
    return errors;
};

const validateProPhone = (values, errors = {}) => {
    if (!values.businessPhone) {
        errors.businessPhone = 'Numéro de téléphone est requis.';
    } else if (values.businessPhone.length < 8) {
        errors.businessPhone = 'Le numéro de téléphone doit comporter au moins 8 caractères.';
    }
    return errors;
};

const validateIntro = (values, errors = {}) => {
    if (!values.introduction) {
        errors.introduction = 'L\'introduction est requis.';
    } else if (values.introduction.length < 80) {
        errors.introduction = 'L\'introduction  doit comporter au moins 80 caractères.';
    }
    return errors;
};

const validateWorkingDays = (values, errors = {}) => {
    if ((!values.workingDaysStarting && values.workingDaysEnding) || (values.workingDaysStarting && !values.workingDaysEnding)) {
        errors.workingDaysStarting = 'Vous devez remplir les deux jours ouvrables de début et de fin ou aucun d\'entre eux';
        errors.workingDaysEnding = 'Vous devez remplir les deux jours ouvrables de début et de fin ou aucun d\'entre eux';
    }

    return errors;
};

const validateLinks = (values, errors = {}) => {
    if (values.internetLinks) {
        let urlsArray = values.internetLinks.split(",");
        urlsArray.map((url, index) => {
            if (!isURL(url)) {
                return errors.internetLinks = "Vous devez entrer des urls valides";
            }
        });
    }
    return errors;
};

const validateBusinessFee = (values, errors = {}) => {
    if ((!values.businessFee && values.businessFeeRate) || (values.businessFee && !values.businessFeeRate)) {
        errors.businessFee = 'Vous devez remplir à la fois les frais et le type de frais ou aucun d\'entre eux';
        errors.businessFeeRate = 'Vous devez remplir à la fois les frais et le type de frais ou aucun d\'entre eux';
    }

    return errors;
};

const validateWorkingHours = (values, errors = {}) => {
    if (!values.workingHoursStarting || !values.workingHoursEnding) {
        errors.workingHoursStarting = 'Les heures de travail sont requises';
        errors.workingHoursEnding = 'Les heures de travail sont requises';
    }

    return errors;
};

const validateForm = (values) => {
    let errors = {};
    errors = validateEmail(values, errors);
    errors = validatePass(values, errors);
    errors = validatePhone(values, errors);
    errors = validatePassTwo(values, errors);
    errors = validateLastName(values, errors);
    errors = validateName(values, errors);

    return errors;
};

const validateProForm = (values) => {
    let errors = {};
    errors = validateProName(values, errors);
    errors = validateProPhone(values, errors);
    errors = validateBusinessFee(values, errors);
    errors = validateWorkingDays(values, errors);
    errors = validateWorkingHours(values, errors);
    errors = validateLinks(values, errors);
    errors = validateIntro(values, errors);
    errors = validateCategoryId(values, errors);
    return errors;
};

const validateUpdateForm = (values) => {
    let errors = {};
    errors = validateEmail(values, errors);
    errors = validatePhone(values, errors);
    errors = validateLastName(values, errors);
    errors = validateName(values, errors);
    return errors;
};

const validateUpdatePass = (values) => {
    let errors = {};
    errors = validatePass(values, errors);
    errors = validateNewPass(values, errors);
    return errors;
};

const validateLoginForm = (values) => {
    let errors = {};

    errors = validateEmail(values, errors);
    errors = validatePass(values, errors);

    return errors;
};

export default {
    getWindowDimensions,
    getSizeType,
    THEME,
    validateForm,
    FORM_THEME,
    validateLoginForm,
    validateUpdateForm,
    validateUpdatePass,
    validateProForm,
    isEqual,
    getBusinessFeeRate
};
