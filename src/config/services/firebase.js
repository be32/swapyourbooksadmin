import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

const settings = {timestampsInSnapshots: true};

const config = {
    apiKey: "AIzaSyAAn6_EQw0_kl910H6buMVyCl4cTL5DRGc",
    authDomain: "mapinfo-49f10.firebaseapp.com",
    databaseURL: "https://mapinfo-49f10.firebaseio.com",
    projectId: "mapinfo-49f10",
    storageBucket: "mapinfo-49f10.appspot.com",
    messagingSenderId: "148332598597",
    appId: "1:148332598597:web:57f0ff813bebeb4c"
};
firebase.initializeApp(config);

firebase.firestore().settings(settings);

export default firebase;
