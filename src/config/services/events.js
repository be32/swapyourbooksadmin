import Firebase from './firebase';

const auth = Firebase.auth();

export const getAllEvents = (callback) => {
    Firebase.firestore().collection('events')
        .onSnapshot(value => {
            if (!value.empty) {
                value.docs.forEach(value1 => {
                    const {name, picture, uid, creation_date, ..._others} = value1.data();

                    uid.get().then((refUserSnapshot) => {
                        let userData = refUserSnapshot.data();
                        // console.log(userData)
                        if (userData) {
                            const {username, ...others} = userData;
                            callback(true, {
                                id: value1.id,
                                name,
                                creation_date,
                                picture,
                                username,
                                uid: others.uid,
                                verified: Boolean(_others.verified)
                            }, null);
                        }
                    });
                    // callback(true, {...value1.data(), id: value1.id}, null);
                });
            } else callback(false, null, null);
        });
};

export const deleteEvent = (id, callback) => {
    Firebase.firestore().collection('events').doc(id)
        .delete().then(() => {
        callback(true, null, null);
    }).catch((err) => {
        callback(false, null, err)
    })
};

export const verifyEvent = (id, verify = true, callback) => {
    Firebase.firestore().collection('events').doc(id)
        .update({verified: verify}).then(() => {
        callback(true, null, null);
    }).catch((err) => {
        callback(false, null, err)
    })
};
