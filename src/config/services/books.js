import Firebase from './firebase';

const auth = Firebase.auth();

export const getAllBooks = (callback) => {
    Firebase.firestore().collection('books')
        .onSnapshot(value => {
        if (!value.empty) {
            value.docs.forEach(value1 => {
                const {name, isbn, toExchange, picture, uid, author, creation_date, ..._others} = value1.data();

                uid.get().then((refUserSnapshot) => {
                    let userData = refUserSnapshot.data();
                    // console.log(userData)
                    const {loc_string, loc, username, ...others} = userData;
                    callback(true, {
                        id: value1.id,
                        name,
                        isbn,
                        toExchange,
                        creation_date,
                        author,
                        picture,
                        loc_string,
                        loc,
                        username,
                        uid: others.uid,
                        verified: Boolean(_others.verified)
                    }, null);
                });
                // callback(true, {...value1.data(), id: value1.id}, null);
            });
        } else callback(false, null, null);
    });
};

export const deleteBook = (id, callback) => {
    Firebase.firestore().collection('books').doc(id)
        .delete().then(() => {
        callback(true, null, null);
    }).catch((err) => {
        callback(false, null, err)
    })
};

export const verifyBook = (id, verify = true, callback) => {
    Firebase.firestore().collection('books').doc(id)
        .update({verified: verify}).then(() => {
        callback(true, null, null);
    }).catch((err) => {
        callback(false, null, err)
    })
};
