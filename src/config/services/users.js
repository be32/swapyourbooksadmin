import Firebase from './firebase';

const auth = Firebase.auth();

export const getCurrentUser = (callback) => {
    const uid = auth.currentUser.uid;
    Firebase.firestore().collection('users').doc(uid)
        .get().then(value => {
        if (value.exists) {
            callback(true, value.data(), null);
        } else callback(false, null, null);
    });
};

export const getAllUsers = (callback) => {
    Firebase.firestore().collection('users')
        .onSnapshot(value => {
        if (!value.empty) {
            value.docs.forEach(value1 => {
                callback(true, {...value1.data(), id: value1.id}, null);
            });
        } else callback(false, null, null);
    });
};

export const deleteUser = (id, callback) => {
    Firebase.firestore().collection('users').doc(id)
        .delete().then(() => {
        callback(true, null, null);
    }).catch((err) => {
        callback(false, null, err)
    })
};

export const suspendUser = (id, suspend = true, callback) => {
    Firebase.firestore().collection('users').doc(id)
        .update({suspended: suspend}).then(() => {
        callback(true, null, null);
    }).catch((err) => {
        callback(false, null, err)
    })
};
