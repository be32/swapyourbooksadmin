import {CONST} from '../config';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import * as storage from 'redux-storage';
import createEngine from 'redux-storage-engine-localstorage';
import reducers from './rootReducer';

const engine = createEngine(CONST.engineKey);
const reducer = storage.reducer(reducers);

const engineMiddleware = storage.createMiddleware(engine);
const middleWares = [thunk, engineMiddleware];

const createStoreWithMiddleware = applyMiddleware(...middleWares)(createStore);
const store = createStoreWithMiddleware(reducer);

const load = storage.createLoader(engine);

load(store)
    .then((newState) => console.log('Loaded state:', newState))
    .catch(() => console.log('Failed to load previous state'));

export default store;
/*const enhancer = compose(applyMiddleware(thunk));

export default createStore(reducers, enhancer);*/
