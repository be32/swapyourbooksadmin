import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {AdminLogin} from "./views/adminLogin";
import {Router, Redirect} from "@reach/router";
import * as PropTypes from 'prop-types';
import {AdminDashboard} from "./views/containers/adminDashboard";
import {UsersList} from "./views/containers/adminDashboard/components/usersList";
import {UsersRequests} from "./views/containers/adminDashboard/components/usersRequests";
import {BooksList} from "./views/containers/adminDashboard/components/booksList";
import {EventsList} from "./views/containers/adminDashboard/components/eventsList";
import Firebase from './config/services/firebase';
import * as UsersService from './config/services/users';

const fireAuth = Firebase.auth();
// "reach router " est notre bibliothèque de routage, ici elle est basée sur la bibliothéque react router,
// et a obtenu des parties plus évoluées comme des routes imbriquées, etc.

// ici nous sommes en train de définir un root guard pour la page de connexion qui redirigera la page
// de l'utilisateur connecté vers le tableau de bord, et lui permettra d'accéder à la page de connexion s'il n'est pas connecté.

class NotLoggedInRoute extends Component {
    render() {
        const {isLoggedIn, children, isAdmin} = this.props;

        return !isLoggedIn && !isAdmin ? children : <Redirect to={'/'} noThrow/>;
    }
}

NotLoggedInRoute.propTypes = {
    isLoggedIn: PropTypes.bool,
    isAdmin: PropTypes.any,
};

// ce composant gardera la partie admin, si l'utilisateur obtient un accès de connexion,
// mais qu'il n'est pas un admin, il sera déconnecté et redirigé vers la page de connexion
class AdminRoute extends Component {
    render() {
        const {isAdmin, isLoggedIn, children} = this.props;

        return isAdmin && isLoggedIn ? children : <Redirect to="/admin-login" noThrow/>;
    }
}

AdminRoute.propTypes = {
    isAdmin: PropTypes.bool,
    isLoggedIn: PropTypes.bool
};


//Le composant "AppContainer" servira ici de gestionnaire d'états pour l'application utilisant les reducers redux

class AppContainer extends Component {
    authUnsubscriber;

    state = {
        isAdmin: false,
        isLoggedIn: false
    };

    componentWillMount(): void {
        this.authUnsubscriber = fireAuth.onAuthStateChanged((user) => {
            if (user) {
                UsersService.getCurrentUser((succ, res, err) => {
                    if (succ) {
                        if (res.isAdmin) {
                            this.setState({
                                isAdmin: true,
                                isLoggedIn: true
                            });
                        } else {
                            this.setState({
                                isAdmin: false,
                                isLoggedIn: true
                            });
                        }
                    } else {
                        this.setState({
                            isAdmin: false,
                            isLoggedIn: false
                        });
                        console.log(err);
                    }
                });
            } else {
                this.setState({
                    isAdmin: false,
                    isLoggedIn: false
                });
            }
        })
    }

    componentWillUnmount(): void {
        if (this.authUnsubscriber)
            this.authUnsubscriber();
    }

    render() {
        const {isAdmin, isLoggedIn} = this.state;

        return (
            <Router>
                <NotLoggedInRoute path={'admin-login'} isAdmin={isAdmin} isLoggedIn={isLoggedIn}>
                    <AdminLogin path={'/'}/>
                </NotLoggedInRoute>
                <AdminDashboard path={'/'}>
                    <AdminRoute isLoggedIn={isLoggedIn} isAdmin={isAdmin} path={'/'}>
                        <UsersList path={'/'}/>
                    </AdminRoute>
                    <AdminRoute isLoggedIn={isLoggedIn} isAdmin={isAdmin} path={'users-requests'}>
                        <UsersRequests path={'/'}/>
                    </AdminRoute>
                    <AdminRoute isLoggedIn={isLoggedIn} isAdmin={isAdmin} path={'books'}>
                        <BooksList path={'/'}/>
                    </AdminRoute>
                    <AdminRoute isLoggedIn={isLoggedIn} isAdmin={isAdmin} path={'events'}>
                        <EventsList path={'/'}/>
                    </AdminRoute>
                </AdminDashboard>
            </Router>
        );
    }
}


function App() {
    return (
        <div className="App">
            <AppContainer/>
        </div>
    );
}

export default App;
